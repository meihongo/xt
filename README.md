# **[XT] XT Development Platform**

---

## [http://cogsmith.com/software/xt][XT-URL]

> **Version:** 3.11

> **Author:** [David Adam Coffey][DAC] <[dac@cogsmith.com][DAC-EMAIL]>

> **Copyright:** [COGSMITH][COGSMITH] © 2008-2013

> **License:** [COGSMITH OPEN LICENSE AGREEMENT [COLA-1.2]][COLA]

---

## **XT** is a development platform built by **[COGSMITH][COGSMITH]**

* **XT.NET**: XT .NET Framework
* **XT.JS**:  XT JavaScript Framework
* **XT.UX**:  XT User Interface Framework

---

## **XT.NET**

#

**XT.NET** is an aplication framework for **.NET**

#

It allows for rapid development of code by leveraging a unified action dispatching system.

It also provides useful extension classes such as logging, configuration, and data access.

#

**Features**

* .NET CLR Extensions
* Assembly Reflection
* Method Invocation
* Application Execution
* Exception Handling
* Logging
* Configuration
* Data Access

---

## **XT.JS**

#

**XT.JS** is a utility toolkit for **JavaScript**

#

**Powered By**

* jQuery
* Underscore / LoDash
* Backbone

#

---

## **XT.UX**

#

**XT.UX** is a user interface framework for **JavaScript**

#

**Powered By**

* Backbone.Marionette
* Handlebars

#

**UI Modules**

* Bootstrap
* ExtJS
* JQM

#

---

[DAC]:http://david-adam-coffey.com
[DAC-EMAIL]:mailto:dac@cogsmith.com
[COGSMITH]:http://cogsmith.com
[COGSMITH-LOGO]:http://cogsmith.com/LOGO_FULL.PNG
[COLA]:http://cogsmith.com/legal/licenses/cola
[XT-URL]:http://cogsmith.com/software/xt
