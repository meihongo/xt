﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Threading;
using System.IO;

namespace XT { public partial class App {

	public static String  LogStream = "STDERR";
	public static Boolean ConsoleClearOnInit = false;
	public static Boolean ExitOnExit = false;
	public static Boolean ThrowOnCrash = false;

	//

	public static String CommandClass = null;

	public static DateTime InitDT = DateTime.Now;

	public static String ExeType; // TODO: make this enum like ExitCode
	
	//

	public static void Sleep (Int32 seconds) { Thread.Sleep(1000*seconds); }

	//

	public static String APP { get { 
		return "APP.APP";
	}}

	//

	public static String LIB { get { 
		String XT_LIB = Environment.GetEnvironmentVariable("XT_LIB");
		if (String.IsNullOrEmpty(XT_LIB)) { return null; } else { return XT_LIB; }		
	}}
	//public static String LIB { get { return @"D:\DEV\CODE\XT\Source\Solution\XT\Projects\XT_DLL\bin\Debug\XT.DLL"; } }



}}